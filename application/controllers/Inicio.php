<?php

defined('BASEPATH') OR exit('No direct script access allowed');

ref::config('validHtml', TRUE);

class Inicio extends CI_Controller{

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		redirect('inicio/entrar');
	}

	public function entrar(){
		$error='';
		if($this->input->post()){
			$u=current(
				Usuario::algunos(
					[
						'usuario'=>$this->input->post('usuario')
					]
				)
			);
			if($u){
				if(password_verify($this->input->post('clave'), $u->clave)){
					$this->session->id_usuario=$u->id;
					redirect('anotador/anotaciones');
				}
				else{
					$error='gordo, te olvidaste la llave?';
					$this->session->aviso='gordo, te olvidaste la llave?';
					$this->session->mark_as_flash('aviso');
				}
			}
			else{
				$error='ni idea quien so\' vo\' gato';
				$this->session->aviso='ni idea quien so\' vo\' gato';
				$this->session->mark_as_flash('aviso');
			}
		}
		if($this->session->id_usuario)
			redirect('anotador/anotaciones');
		$this->load->view(
			'form_login',
			[
				'error'=>$error
			]
		);
	}

}