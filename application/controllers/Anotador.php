<?php

defined('BASEPATH') OR exit('No direct script access allowed');

ref::config('validHtml', TRUE);

class Anotador extends CI_Controller{

	public function _remap($method,$params=[]){
		if( ! $this->session->id_usuario )
			redirect('inicio/entrar');
		call_user_func_array(array($this,$method),$params);
	}

	public function _output($output){
		echo $output,
			PHP_EOL,
			'<!-- generado en ',
			(microtime(TRUE)-$_SERVER['REQUEST_TIME_FLOAT']),
			' segundos -->'
		;
	}

	public function __construct(){
		parent::__construct();
		$this->load->model('Anotacion');
	}

	public function index(){
		redirect('anotador/anotaciones');
	}

	public function anotaciones($pagina=null){
		$pagina=abs( (int) $pagina );
		if(!$pagina)$pagina=1;
		$por_pagina=5;
		$anotaciones=Anotacion::todos($pagina,$por_pagina);
		$contador=Anotacion::contar();
		$this->load->library('pagination');
		$this->pagination->initialize([
			'base_url'=>site_url('anotador/anotaciones')
			,'total_rows'=>$contador
			,'per_page'=>$por_pagina
		]);
		$this->load->view(
			'notero/listado',
			[
				'anotaciones'=>$anotaciones,
			]
		);
	}

	public function anotacion($id=0){
		$a=new Anotacion($id);
		if( ! $id )
			$a->id_usuario=$this->session->id_usuario;
		$this->load->view(
			'notero/editor',
			[
				'anotacion'=>$a
			]
		);
	}

	public function migrador(){
		$this->load->library('migration');
		if( ! $this->migration->current() )
			show_error( $this->migration->error_string() );
		else
			r( $this->migration->current() );
	}

	public function guardar(){
		$a=new Anotacion($this->input->post('id'));
		$a->titulo=$this->input->post('titulo');
		$a->texto=$this->input->post('texto');
		$a->id_usuario=$this->input->post('id_usuario');
		if( $a->guardar() ){
			$this->session->aviso='se guardo la anotación';
			$this->session->mark_as_flash('aviso');
			redirect('anotador/anotaciones');
		}
		else{
			r( $this->input->post() );
			r( $a );
		}
	}

}
