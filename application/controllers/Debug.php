<?php

defined('BASEPATH') OR exit('No direct script access allowed');

ref::config('validHtml', TRUE);

class Debug extends CI_Controller{

	public function __construct(){
		parent::__construct();
	}

	public function r_this(){

		r( $this );

		// r( ENVIRONMENT );

		// r( $this->uri->ruri_string() );
		// r( $this->uri->uri_string() );

		// r( $this->uri->rsegment_array() );
		// r( $this->uri->segment_array() );

		// r( $this->uri->ruri_to_assoc(3, ['b','c']) );
		// r( $this->uri->uri_to_assoc(3, ['b','c']) );
		// r( $this->uri->uri_to_assoc(2, ['b','c']) );
	}

	public function subir_archivo(){
		if( ! $this->session->id_usuario )
			redirect('inicio/entrar');

/*
		if( $_FILES ){
			if( $_FILES['archivo'] ){
				if($_FILES['archivo']['error']){
					$errores=[
						0=>'UPLOAD_ERR_OK',
						1=>'UPLOAD_ERR_INI_SIZE',
						2=>'UPLOAD_ERR_FORM_SIZE',
						3=>'UPLOAD_ERR_PARTIAL',
						4=>'UPLOAD_ERR_NO_FILE',
						6=>'UPLOAD_ERR_NO_TMP_DIR',
						7=>'UPLOAD_ERR_CANT_WRITE',
						8=>'UPLOAD_ERR_EXTENSION',
					];
					$this->session->aviso='error al subir el archivo: '.$errores[$_FILES['archivo']['error']];
					$this->session->mark_as_flash('aviso');
				}
				elseif(!$_FILES['archivo']['size']){
					$this->session->aviso='¿subiste un archivo vacio?';
					$this->session->mark_as_flash('aviso');
				}
				elseif(!is_uploaded_file($_FILES['archivo']['tmp_name'])){
					$this->session->aviso='saquese de aca kaker sucio';
					$this->session->mark_as_flash('aviso');
				}
				elseif(!move_uploaded_file($_FILES['archivo']['tmp_name'],getcwd().'/adjuntos/'.$_FILES['archivo']['name'])){
					$this->session->aviso='no se pudo guardar el archivo';
					$this->session->mark_as_flash('aviso');
				}
				else{
					$this->session->aviso='ok, se guardo el archivo';
					$this->session->mark_as_flash('aviso');
				}
				redirect('debug/subir_archivo');
			}
		}
*/

		$this->load->library('upload');
		if($_FILES){
			$this->upload->initialize([
				'upload_path'=>'./adjuntos',
				'allowed_types'=>'pdf',
				'overwrite'=>true,
			]);
			if( $this->upload->do_upload($field='archivo') ){
				$this->session->aviso='ok, se guardo el archivo';
				$this->session->mark_as_flash('aviso');
				redirect('debug/subir_archivo');
			}
		}

		$this->load->view(
			'notero/subir_archivo'
		);
	}

}
