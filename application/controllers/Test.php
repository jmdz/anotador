<?php

defined('BASEPATH') OR exit('No direct script access allowed');

ref::config('validHtml', TRUE);

class Test extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->library('unit_test');
        $this->load->model('Anotacion');

        $this->unit->set_template(
            '<table border="1" cellpadding="4" cellspacing="0">'
                .'{rows}<tr>'
                    .'<td>{item}</td>'
                    .'<td>{result}</td>'
                .'</tr>{/rows}'
            .'</table><br/>'
        );

        $this->unit->set_test_items(['test_name', 'result']);
    }

    public final function recuperar_anotaciones($report=true){

        $this->unit->run(
            Anotacion::todos()
            ,true
            ,'todos'
            ,'tambien vale como test de "algunos" sin argumentos'
        );

        $this->unit->run(
            count(Anotacion::algunos(['id'=>1]))
            ,1
            ,'uno'
            ,'id=1 devuelva solo un registro'
        );

        $this->unit->run(
            new Anotacion(1)
            ,Anotacion::uno(['id'=>1])
            ,'new'
            ,'id=1'
        );

        $this->unit->run(
            Anotacion::algunos(['titulo'=>'Lorem Ipsum: 5p'])
            ,Anotacion::algunos(['id'=>1])
            ,'algunos'
            ,'que titulo="Lorem Ipsum: 5p" devuelva lo mismo que id=1'
        );

        $this->unit->run(
            Anotacion::contar(['titulo'=>'Lorem Ipsum: 5p'])
            ,'is_int'
            ,'contar'
            ,'titulo="Lorem Ipsum: 5p"'
        );

        if($report)
            echo $this->unit->report();
    }

    public final function editar_anotaciones($report=true){

        $d=date('c');

        // $a=new Anotacion;
        // $a->titulo="prueba de inserción $d";
        // $a->texto='prueba de inserción prueba de inserción prueba de inserción prueba de inserción prueba de inserción prueba de inserción prueba de inserción prueba de inserción';
        // $a->id_usuario=1;
        // $g=$a->guardar();
        // $this->unit->run(
        //  $g
        //  ,'is_int'
        //  ,'insertar'
        //  ,'tit "prueba de inserción $d", text "prueba de inserción prueba de inserción [...]", id_usuario "1"'
        // );

        $this->unit->run(
            Anotacion::instanciar()
                ->__set('titulo',"prueba de inserción $d")
                ->__set('texto','prueba de inserción prueba de inserción prueba de inserción prueba de inserción prueba de inserción prueba de inserción prueba de inserción prueba de inserción')
                ->__set('id_usuario','1')
                ->guardar()
            ,'is_int'
            ,'insertar'
            ,'tit "prueba de inserción $d", text "prueba de inserción prueba de inserción [...]", id_usuario "1"'
        );

        $this->unit->run(
            count(Anotacion::algunos(['titulo LIKE "%'.$d.'"'=>null]))
            ,12 // para que falle
            ,'recuperar'
            ,'que titulo LIKE "%$d" devuelva un solo registro'
        );

        $this->unit->run(
            Anotacion::uno(['titulo LIKE "%'.$d.'"'=>null])
                ->__set('texto','texto modificado')
                ->guardar()
            ,'is_numeric'
            ,'recuperar'
            ,'actualizamos el texto'
        );

        $borrar = function(){
            $CI=&get_instance();
            return $CI->db->delete(
                'anotaciones'
                ,[
                                // 2019-09-09T03:40:53+02:00
                    'titulo LIKE "%2019-09-__T__:__:_____:__"'=>null
                ]
            );
        };

        $this->unit->run(
            $borrar()
            ,true
            ,'borrar'
            ,'solo para no juntar mugre'
        );

        if($report)
            echo $this->unit->report();
    }


    public function index(){
        $reflexion=new ReflectionClass($this);
        $metodos=$reflexion->getMethods(ReflectionMethod::IS_FINAL);
        foreach($metodos as $m){
            $this->{$m->name}(false);
        }unset($m);
        $this->unit->set_test_items([
            'test_name',
            'test_datatype',
            'res_datatype',
            'result',
            'file',
            'line',
            'notes',
        ]);
        $this->load->library('parser');
        echo '<style>
                table,tr,th,td{border:thin solid;border-collapse:collapse}
                th{background:silver;}
                tr.Failed{background:red;}
            </style>
            <table>
            <tr>
                <th>Test Name</th>
                <th>Test Datatype</th>
                <th>Expected Datatype</th>
                <th>Result</th>
                <th>File Name</th>
                <th>Line Number</th>
                <th>Notes</th>
            </tr>'
            ,$this->parser->parse_string(
                '{pruebas}<tr class="{Result}">
                    <td>{Test Name}</td>
                    <td>{Test Datatype}</td>
                    <td>{Expected Datatype}</td>
                    <td>{Result}</td>
                    <td>{File Name}</td>
                    <td>{Line Number}</td>
                    <td>{Notes}</td>
                </tr>{/pruebas}'
                ,['pruebas'=>$this->unit->result()]
                ,true
            )
            ,'</table>'
        ;

    }

}
