<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_crear_anotador extends CI_Migration{

	public function __constructor(){
		$this->load->dbforge();
	}

	public function up(){
		$this->dbforge
			->add_field('id')
			->add_field(
				[
					'titulo'=>[
						'type'=>'VARCHAR',
						'constraint'=>126,
						'null'=>false,
					],
					'texto'=>[
						'type'=>'TEXT',
						'null'=>false,
					],
				]
			)
			->create_table('anotaciones')
		;
	}

	public function down(){
		$this->dbforge->drop_table('anotaciones');
	}

}