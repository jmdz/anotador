<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_crear_anotador_aa extends CI_Migration{

	public function __constructor(){
		$this->load->dbforge();
	}

	public function up(){
		$tabla='anotaciones';
		crear_tabla_aa($tabla);
	}

	public function down(){
		$tabla='anotaciones';
		$this->dbforge->drop_table("{$tabla}_aa");
	}

}