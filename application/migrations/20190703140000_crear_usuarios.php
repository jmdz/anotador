<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_crear_usuarios extends CI_Migration{

	public function __constructor(){
		$this->load->dbforge();
	}

	public function up(){
		$this->dbforge
			->add_field('id')
			->add_field(
				[
					'usuario'=>[
						'type'=>'VARCHAR',
						'constraint'=>18,
						'null'=>false,
					],
					'clave'=>[
						'type'=>'VARCHAR',
						'constraint'=>255,
						'null'=>false,
					],
				]
			)
			->add_key('usuario')
			->create_table('usuarios')
		;
		$this->db->insert(
			'usuarios',
			[
				'usuario'=>'sadmin',
				'clave'=>password_hash('admin123',PASSWORD_DEFAULT),
			]
		);
		$this->dbforge
			->add_column(
				'anotaciones',
				[
					'id_usuario'=>[
						'type'=>'INT',
						'constraint'=>9,
						'null'=>false,
					],
				]
			)
		;
		$this->db->update(
				'anotaciones',
				[
					'id_usuario'=>1,
				]
				// suena raro pero es correcto que este update sea sin where :P
		);
	}

	public function down(){
		$this->dbforge->drop_table('usuarios');
		$this->dbforge->drop_column(
			'anotaciones',
			'id_usuario'
		);
	}

}