<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Anotacion extends Coop_Model{

	const TABLA='anotaciones';

	use AutoAud;

	protected $id='',
		$titulo='',
		$texto='',
		$id_usuario='';

	private $usuario;

	public function __get($prop_name){
		switch ($prop_name) {
			case 'usuario':
				if($this->usuario===null)
					$this->usuario=new Usuario($this->id_usuario);
				return $this->usuario;
		}
		return parent::__get($prop_name);
	}

	public function __set($prop_name,$value){
		switch ($prop_name) {
			case 'id':
				return $this;
			case 'titulo':
				$value=strip_tags($value);
		}
		return parent::__set($prop_name,$value);
	}

}
