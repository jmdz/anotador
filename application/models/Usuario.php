<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends Coop_Model{

	const TABLA='usuarios';

	protected $id='',
		$usuario='',
		$clave='';

	public function __toString(){
		return $this->usuario;
	}

	public function __get($prop_name){
		switch ($prop_name) {
		}
		return parent::__get($prop_name);
	}

	public function __set($prop_name,$value){
		switch ($prop_name) {
			case 'id':
				return $this;
		}
		return parent::__set($prop_name,$value);
	}

}
