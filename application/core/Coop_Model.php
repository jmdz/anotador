<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coop_Model extends CI_Model{

	const TABLA='';
	const ID='id';

	public static function instanciar($id=0){
		return new static($id);
	}

	public static function uno($where=[]){
		return current(static::algunos($where,1,1));
	}

	public static function todos($pagina=0,$por_pagina=10){
		return static::algunos([],$pagina,$por_pagina);
	}

	public static function algunos_old($where=[]){
		$o=[];
		get_instance()->db
			->select()
			->from(static::TABLA)
		;
		foreach ($where as $k => $v) {
			get_instance()->db->where($k,$v);
		}unset($k,$v);
		$r=get_instance()->db
			->get()
			->result()
		;
		foreach ($r as $i) {
			$t=new static;
			foreach ($i as $k => $v) {
				$t->$k=$v;
			}unset($k, $v);
			$o[]=$t;
			unset($t);
		}unset($i);
		return $o;
	}

	public static function algunos($where=[],$pagina=0,$por_pagina=10){
		get_instance()->db
			->select()
			->from(static::TABLA)
		;

		foreach ($where as $k => $v) {
			get_instance()->db->where($k,$v);
		}unset($k,$v);

		if($pagina){
			$pagina=($pagina-1)*$por_pagina;
			get_instance()->db->limit($por_pagina,$pagina);
		}

		$q=get_instance()->db->get();

		return $q
			?$q->result(static::class)
			:[]
		;
	}

	public static function contar($where=[]){
		get_instance()->db
			->select()
			->from(static::TABLA)
		;

		foreach ($where as $k => $v) {
			get_instance()->db->where($k,$v);
		}unset($k,$v);

		return get_instance()->db->count_all_results();
	}

	public function __construct($id=0){
		parent::__construct();
		$this->load->database();
		if($id){
			$i=static::ID;
			$r=$this->db
				->select()
				->from(static::TABLA)
				->where([
					$i=>$id,
				])
				->limit(1)
				->get()
				->row()
			;
			foreach ($r as $k => $v) {
				$this->$k = $v;
			}unset($k, $v);
		}
	}

	public function __get($prop_name){
		return isset($this->$prop_name)
			? $this->$prop_name
			: parent::__get($prop_name)
		;
	}

	public function __set($prop_name,$value){
		$this->$prop_name=$value;
		return $this;
	}

	public function guardar(){
		$data=[];
		foreach($this as $k=>$v){
			$data[$k]=$v;
		}unset($k,$v);
		$i=static::ID;
		unset($data[$i]);
		if($this->$i){
			$r=$this->db->update(
				static::TABLA,
				$data,
				[
					$i=>$this->$i,
				]
			);
		}
		else{
			$r=$this->db->insert(
				static::TABLA,
				$data
			);
			if($r)
				$this->$i=$this->db->insert_id();
		}
		return $r ? $this->$i : false;
	}

}

trait AutoAud{
	public function guardar(){
		$ident=static::ID;
		$clase=new ReflectionClass($this);
		$data=[
			// 'aa_id'=>null,
			'aa_accion'=>$this->$ident?'UPDATE':'INSERT',
			'aa_fechahora'=>date('Y-m-d H:i:s'),
			'aa_idusuario'=>(int)$this->session->id_usuario,
			'aa_ipaddress'=>$this->input->ip_address(),
		];
		if(parent::guardar()){
			foreach($clase->getProperties() as $prop){
				if(
					$clase->name === $prop->class
					&& $prop->isProtected()
					&& $prop->isDefault()
					&& ! $prop->isStatic()
				){
					$data[$prop->name]=$this->{$prop->name};
				}
			}unset($prop);
			if($this->db->insert(static::TABLA.'_aa',$data)){
				return $this->$ident;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}
}

trait AutoAudAA{
	// aca irian los get/set hoshibles
}
