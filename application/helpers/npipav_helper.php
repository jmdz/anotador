<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('e')){
	function e(...$a){
		foreach($a as $v){
			echo htmlspecialchars($v,ENT_QUOTES|ENT_HTML5);
		}unset($v);
	}
}

if(!function_exists('crear_tabla_aa')){
	function crear_tabla_aa($tabla){
		$CI=&get_instance();
		$CI->load->dbutil();
		{$create=$CI->dbutil->backup([
			'tables'=>$tabla,
			'add_drop'=>false,
			'add_insert'=>false,
			'format'=>'txt',
		]);}
		{$insert=substr($CI->dbutil->backup([
			'tables'=>$tabla,
			'add_drop'=>false,
			'add_insert'=>true,
			'format'=>'txt',
		]),strlen($create));}
		{$create=str_replace(
			[
				"# TABLE STRUCTURE FOR: {$tabla}",
				" AUTO_INCREMENT,",
				"CREATE TABLE `{$tabla}` (",
				"  PRIMARY KEY (`",
			]
			,[
				"# TABLE STRUCTURE FOR: {$tabla}_aa",
				",",
				"CREATE TABLE `{$tabla}_aa` (\n"
					."  `aa_id` BIGINT(18) NOT NULL AUTO_INCREMENT,\n"
					."  `aa_accion` ENUM('INSERT','DELETE','UPDATE') NOT NULL DEFAULT 'INSERT',\n"
					."  `aa_fechahora` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,\n"
					."  `aa_idusuario` BIGINT(18) NOT NULL DEFAULT '0',\n"
					."  `aa_ipaddress` VARCHAR(45) NOT NULL DEFAULT '0.0.0.0',",
				"  PRIMARY KEY (`aa_id`),\n  KEY (`",
			]
			,$create
		);
		$create=preg_replace(
			[
				"~ AUTO_INCREMENT=[0-9]+~",
			]
			,[
				"",
			]
			,$create
		);}
		if($CI->db->simple_query($create)){
			{$insert=str_replace(
				[
					"INSERT INTO `{$tabla}`",
				]
				,[
					"INSERT INTO `{$tabla}_aa`",
				]
				,$insert
			);
			$insert=preg_replace(
				[
					// "~INSERT INTO `{$tabla}` \\(([^)]+)\\) VALUES \\([^)]+\\);~"
				]
				,[
					// "INSERT INTO `{$tabla}_aa` (\\1) SELECT \\1 FROM `{$tabla}`;"
				]
				,$insert
			);}
			if($CI->db->simple_query($insert))
				return true;
			else
				return false;
		}
		else{
			return false;
		}
	}
}
