<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config=array(

	'base_url'=>null
	,'total_rows'=>null
	,'per_page'=>null

	,'uri_segment'=>3
	,'num_links'=>2
	,'use_page_numbers'=>true
	,'page_query_string'=>false
	,'reuse_query_string'=>true
	,'prefix'=>''
	,'suffix'=>''
	,'use_global_url_suffix'=>false

	,'full_tag_open'=>'<ul class="pagination justify-content-center">'
		,'first_tag_open'=>'<li class="page-item first" aria-label="first">'
			,'first_url'=>''
			,'first_link'=>'<span aria-hidden="true">⏪</span>'
		,'first_tag_close'=>'</li>'
		,'prev_tag_open'=>'<li class="page-item prev" aria-label="prev">'
			,'prev_link'=>'<span aria-hidden="true">⏴</span>'
		,'prev_tag_close'=>'</li>'
		,'display_pages'=>true
		,'cur_tag_open'=>'<li class="page-item active"><a class="page-link">'
		,'cur_tag_close'=>'</a></li>'
		,'num_tag_open'=>'<li>'
		,'num_tag_close'=>'</li>'
		,'next_tag_open'=>'<li class="page-item next" aria-label="next">'
			,'next_link'=>'<span aria-hidden="true">⏵</span>'
		,'next_tag_close'=>'</li>'
		,'last_tag_open'=>'<li class="page-item last" aria-label="last">'
			,'last_link'=>'<span aria-hidden="true">⏩</span>'
		,'last_tag_close'=>'</li>'
	,'full_tag_close'=>'</ul>'

	,'attributes'=>array(
		'class'=>'page-link',
		// 'rel'=>false,
	)

);
