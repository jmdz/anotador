<?php $this->load->view('cabeza', [ 'titulo'=>'Mi Anotador :: Identificate' ]) ?>

<div class="row justify-content-center">
	<div class="col-12 col-sm-12 col-md-2 mt-3">
		<!-- <p class="text-center"><?= $error ?></p> -->
		<form method="post">
			<input class="form-control text-center" name="usuario" type="text" placeholder="usuario" />
			<input class="form-control text-center" name="clave" type="password" placeholder="contraseña" />
			<input class="btn btn-block btn-primary" type="submit" value="entrar" />
		</form>
	</div>
</div>

<?php $this->load->view('pie') ?>