<!DOCTYPE html>
<html class="h-100" lang="es-AR" dir="ltr">
<head>
<meta charset="utf-8"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Content-Script-Type" content="application/javascript"/>
<meta http-equiv="Content-Style-Type" content="text/css"/>
<meta http-equiv="Content-Languaje" content="es-AR"/>
<meta name="viewport" content="width=device-width,initial-scale=1"/>
<meta name="language" content="es-AR"/>
<link rel="icon" type="image/x-icon" sizes="16x16 32x32 48x48" href="http://static.jmdz.com.ar//favicon.ico"/>
<link rel="apple-touch-icon-precomposed" type="image/png" sizes="152x152" href="http://static.jmdz.com.ar//apple-touch-icon-precomposed.png"/>
<meta name="msapplication-TileImage" type="image/png" sizes="144x144" content="http://static.jmdz.com.ar//favicon-144.png"/>
<title><?= $titulo ?></title>
<meta name="author" content="Mauro Daino (jMdZ)"/>
<meta name="copyright" content="© 2019, Mauro Daino"/>
<meta name="description" content=""/>
<meta name="keywords" content=""/>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
<link rel="stylesheet" href="http://static.jmdz.com.ar/libs/jmdz-fix-bs.css"/>
<script src="http://static.jmdz.com.ar/libs/jmdz-fix-js.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="http://static.jmdz.com.ar/libs/jmdz-fix-jq.js" async></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="http://static.jmdz.com.ar/libs/jmdz-fix-bs.js" defer></script>
</head>
<body class="fix-bs h-100 d-flex flex-column -highlight-external-links">
<header>
	<nav class="navbar navbar-dark bg-dark justify-content-start">
		<a class="navbar-brand" href="/fh/jmdz.com.ar/">
			<img alt="isotipo" class="align-text-bottom" src="http://static.jmdz.com.ar/images/isotipo-tt-ww-t.png" style="width:2em;"/>
			<span class="h1">jMdZ</span>
		</a>
	</nav>
</header>

<section class="container-fluid">

<div class="row">
	<div class="col-12 col-sm-12 mt-3">
		<h2 class="text-center"><?= $titulo ?></h2>
	</div>
</div>
