<script type="text/javascript">
	$(document).ready(function(){
		let aviso='<? e(get_instance()->session->aviso) ?>';
		if(aviso){
			bs_modal({
				close:false,
				body:aviso,
			});
		}
	});
</script>

<div class="row">
	<div class="col mt-3">
	</div>
</div>

</section>

<a class="arrow-top-after text-light arrow-no-margin mx-3 text-5 cursor-hand" data-nice-link-up></a>

<footer class="container-fluid mt-auto">
	<div class="row text-light bg-dark">
		<p class="col mt-3">© 2019, Mauro Daino</p>
	</div>
</footer>

</body>
</html>