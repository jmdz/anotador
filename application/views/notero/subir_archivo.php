<?php $this->load->view('cabeza', [ 'titulo'=>'Mi Anotador :: Subir Archivo' ] ) ?>
<form enctype="multipart/form-data" method="post">
	<div class="row">
		<div class="col-12">
			<?= get_instance()->upload->display_errors() ?>
			<!-- <input type="hidden" name="MAX_FILE_SIZE" value="10240" data-eso-del-value-son="10KiB"/> -->
			<input type="file" name="archivo"/>
			<br/>
			<input type="submit" value="subir"/>
		</div>
	</div>
</form>
<?php $this->load->view('pie') ?>
