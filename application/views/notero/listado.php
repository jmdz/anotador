<?php $this->load->view('cabeza', [ 'titulo'=>'Mi Anotador :: Listado' ]) ?>

<div class="row mt-3 mb-3">
	<div class="col-12">
		<table class="table table-bordered table-hover table-striped">
			<thead class="thead-dark">
				<tr>
					<th>ID</th>
					<th>Titulo</th>
					<th>Texto</th>
					<th>Usuario</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($anotaciones as $e) { ?>
				<tr>
					<td class="text-center"><? e($e->id) ?></td>
					<td class=""><? e($e->titulo) ?></td>
					<td class=""><? e($e->texto) ?></td>
					<td class=""><? e($e->usuario) ?></td>
					<td class="text-center"><a href="<?= site_url( [ 'anotador/anotacion', $e->id ] ) ?>">abrir</a></td>
				</tr>
				<?php }unset($e) ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="99"><a href="<?= site_url(['anotador/anotacion','0']) ?>">nuevo</a></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>

<div class="row mt-3 mb-3">
	<div class="col-12">
<nav class="text-center">
<?= $this->pagination->create_links() ?>
</nav>
	</div>
</div>

<!--<div class="row">
	<div class="col-12 mt-3">
		<?php // r( $anotaciones ) ?>
	</div>
</div>-->

<?php $this->load->view('pie') ?>
