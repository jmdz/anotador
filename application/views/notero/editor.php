<?php $this->load->view('cabeza', [ 'titulo'=>'Mi Anotador :: Editor' ] ) ?>

<form
	method="post"
	class="horizontal"
	action="<?= site_url('anotador/guardar') ?>"
	>

	<div
		class="row mt-3"
		>
		<div
			class="col-12 col-sm-12 col-md-2"
			>
			<label
				for="id"
				>
				ID
			</label>
			<input
				class="form-control"
				id="id"
				name="id"
				readonly="readonly"
				type="text"
				value="<? e($anotacion->id) ?>"
			/>
		</div>
		<div
			class="col-12 col-sm-12 col-md-8"
			>
			<label
				for="titulo"
				>
				Titulo
			</label>
			<input class="form-control" id="titulo" name="titulo" type="text" required="required" value="<? e($anotacion->titulo) ?>"/>
			<small class="text-muted">Solo texto</small>
		</div>
		<div
			class="col-12 col-sm-12 col-md-2"
			>
			<input
				name="id_usuario"
				type="hidden"
				value="<? e($anotacion->id_usuario) ?>"
				/>
			<label
				for="id"
				>
				Usuario
			</label>
			<input
				class="form-control"
				id="usuario"
				readonly="readonly"
				type="text"
				value="<? e($anotacion->usuario) ?>"
			/>
		</div>
	</div>

	<div
		class="row mt-3"
		>
		<div
			class="col-12 col-sm-12 col-md-12"
			>
			<label
				for="texto"
				>
				Texto
			</label>
			<textarea
				class="form-control"
				id="texto"
				required="required"
				name="texto"
				><? e($anotacion->texto) ?>
			</textarea>
		</div>
	</div>

	<div
		class="row mt-3"
		>
		<div
			class="col-12 col-sm-12 col-md-12"
			>
			<input
				class="btn btn-primary"
				type="submit"
				value="Guardar"
			/>
			<input
				class="btn btn-secondary"
				type="button"
				value="Cerrar"
				onclick="history.back()"
			/>
		</div>
	</div>
</form>

<!--<div class="row">
	<div class="col-12 mt-3">
		<?php // r($anotacion) ?>
	</div>
</div>-->

<?php $this->load->view('pie') ?>